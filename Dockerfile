FROM alpine:latest

ARG STEP_TGZ

RUN apk add --no-cache docker-cli

COPY ./deploy.sh /deploy.sh
COPY ./entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
