#!/bin/sh

# Usage: /deploy.sh /path/to/myservice/stack.yml

# The yml stack file containing the service
STACK=$1

DIR="$(dirname "${STACK}")"
SVC="$(basename "${DIR}")"

ENV_FILE="${STACK%.*}.env"
if [ -f "${ENV_FILE}" ]; then
    echo "Sourcing environment from ${ENV_FILE}"
    . "${ENV_FILE}"
fi  

# no pushd in alpine
cd "${DIR}"
docker stack deploy -c "${STACK}" "${SVC}"
