#!/bin/sh

# mount source on host; pass through to children who need it
export SERVICE_DIR="${SERVICE_DIR}"
SERVICE_NAME="${SERVICE_NAME:-autostackman}"

if [ "$(id -u)" = "0" ]; then
    # Make sure our low-priv user can run docker commands,
    #   then switch to them
    # https://github.com/jenkinsci/docker/issues/196#issuecomment-179486312
    # https://vitorbaptista.com/how-to-access-hosts-docker-socket-without-root

    SOCK="/var/run/docker.sock"
    GRP="docker"
    USR="docker"

    adduser -S "${USR}"
    if [ -S "${SOCK}" ]; then
        GID=$(stat -c '%g' "${SOCK}")
        addgroup -g "${GID}" "${GRP}"
        addgroup "${USR}" "${GRP}"
    fi

    # Re-exec as low-priv user
    su -s /bin/sh "${USR}" -c "$0 $@"
else
    DEPLOY="/deploy.sh"
    for SERVICE in $(find "${SERVICE_DIR}" -name stack.yml); do
        DIR="$(dirname "${SERVICE}")"
        # Busybox has no -empty
        CERTS="$(find "${DIR}" -name "*.crt")"
        SKIP=0
        for CERT in ${CERTS}; do
            if [ ! -s "${CERT}" ]; then
                echo "${SERVICE} requires missing ${CERT}"
                SKIP=1
            fi
        done
        if [ "$(dirname "${SERVICE}")" = "${SERVICE_NAME}" ] || [ -f "${DIR}/${SERVICE_NAME}.ignore" ]; then
            # Don't redeploy self or other special services
            SKIP=1
        fi
        if [ "${SKIP}" = "1" ]; then
            echo "Skipping ${SERVICE}"
        else
            "${DEPLOY}" "${SERVICE}" 
        fi
    done
    echo "Bootstrap complete, removing self"
    docker service rm "${SERVICE_NAME}_${SERVICE_NAME}"
    # Wait to be deleted
    sleep infinity
fi
